﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    private static bool created = false;

    private GameObject player;

    public int prevScore = 0;
    public int highScore = 0;
    public int coins = 0;

    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
            //Debug.Log("Awake: " + this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {


	}
	
	// Update is called once per frame
	void Update () {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            if (player != null)
                prevScore = 0;
        }
        if (player != null)
        {
            if (player.transform.position.y > prevScore)
            {
                prevScore = Mathf.RoundToInt(player.transform.position.y);
                HighScore(prevScore);
            }
        }
    }

    private void HighScore(int _score)
    {
        if (prevScore > GameObject.Find("GameManager").GetComponent<GameManager>().highScore)
            GameObject.Find("GameManager").GetComponent<GameManager>().highScore = prevScore;
    }
}
