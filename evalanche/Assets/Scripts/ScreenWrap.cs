﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWrap : MonoBehaviour {
    public float leftConstraint = 0.0f;
    public float rightConstraint = 960.0f;
    public float buffer = 1.0f; // set this so the spaceship disappears offscreen before re-appearing on other side

    void Start()
    {
        // set Vector3 to ( camera left/right limits, spaceship Y, spaceship Z )
        // this will find a world-space point that is relative to the screen
        leftConstraint = GetWorldPositionOnPlane(new Vector3(0, 0.0f, 0.0f), 0).x; // Or set to (0,0,0)
        rightConstraint = leftConstraint * -1;
        //rightConstraint = GetWorldPositionOnPlane(new Vector3(Camera.main.rect.width, 0.0f, 0.0f), 0).x; // Or set to (Screen.width,0,0)
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ViewportPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }



    void Update()
    {
        if (this.GetComponent<Transform>().position.x < leftConstraint - buffer) { // ship is past world-space view / off screen
            this.GetComponent<Transform>().position = new Vector2(rightConstraint + buffer, this.GetComponent<Transform>().position.y);  // move ship to opposite side
        }
        if (this.GetComponent<Transform>().position.x > rightConstraint + buffer) {
            this.GetComponent<Transform>().position = new Vector2(leftConstraint - buffer, this.GetComponent<Transform>().position.y);  // move ship to opposite side
        }
    }

}
