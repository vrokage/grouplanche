﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class block : MonoBehaviour {
    private GameObject player;
    private GameObject lava;

    public float sizeVariation = 0;
    public bool enableXVariation = false;
    public bool enableYVariation = false;
    //public bool isCoin = false;
    public bool isSafe = false;
    public bool stayDynamic = false;

    public int blockHealth = 1;
    [HideInInspector] public int blockHealthCurrent;
    public Material damagedMaterial;

    public GameObject rubbleObj;
    public int rubbleSpawnedMin = 0;
    public int rubbleSpawnedMax = 0;
    public float rubbleForceXMax = 1000f;
    private float rubbleForceYMin = 500;
    private float rubbleForceYMax = 4000f;

    [HideInInspector] public float sleepTimer = 1f;
    [HideInInspector] public float sleepTimerClock = 1f;
    private float DestroyTimer = 10f;
    private bool startDestroyTimer = false;

    // Use this for initialization
    void Start () {
        if (rubbleSpawnedMax < rubbleSpawnedMin)
            rubbleSpawnedMax = rubbleSpawnedMin;
        blockHealthCurrent = blockHealth;
        float random = Random.Range(-1 * sizeVariation, sizeVariation);
        if (enableXVariation)
            this.transform.localScale = new Vector3(this.transform.localScale.x + random, this.transform.localScale.y, this.transform.localScale.z);
        if (enableYVariation)
            this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y + random, this.transform.localScale.z);
        player = GameObject.FindGameObjectWithTag("Player");
        lava = GameObject.FindGameObjectWithTag("lava");
    }

    private void Update()
    {
        if (this.transform.position.y < lava.GetComponent<Collider2D>().bounds.center.y + lava.GetComponent<Collider2D>().bounds.extents.y)
        {
            this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            startDestroyTimer = true;
        }
        if (startDestroyTimer)
            DestroyTimer -= Time.deltaTime;
        if (DestroyTimer <= 0)
            Destroy(this.gameObject);
        if (blockHealthCurrent <= 0)
            Die();

        if (Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.y) <= 0.01 && !stayDynamic)
        {
            sleepTimerClock -= Time.deltaTime;            
        }
        else
        {
            //sleepTimerClock = sleepTimer;
        }

        if (sleepTimerClock <= 0)
        {
            this.GetComponent<Rigidbody2D>().Sleep();
        }
        else
        {
            this.GetComponent<Rigidbody2D>().WakeUp();
            this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }

        if (!this.GetComponent<Rigidbody2D>().IsAwake() || startDestroyTimer)//this.transform.position.y < player.transform.position.y             
        {
            if (!stayDynamic)
                this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }
    }

    public void Die()
    {
        Renderer rend = GetComponent<Renderer>();

        Vector3 spawnPos = rend.bounds.center;
        float scaleY = rend.bounds.extents.y/2;
        Destroy(this.gameObject);
        if (rubbleObj != null)
        {
            int rubbleSpawned = Random.Range(rubbleSpawnedMin, rubbleSpawnedMax+1);
            for (int i = 0; i < rubbleSpawned; i++)
            {
                spawnPos = new Vector3(spawnPos.x/* + Random.Range(-1 * scaleX, scaleX)*/, spawnPos.y + Random.Range(0, scaleY), spawnPos.z);
                GameObject rubble = Instantiate(rubbleObj, spawnPos, Quaternion.identity);

                float rubbleForceX = Random.Range(-rubbleForceXMax, rubbleForceXMax);
                float rubbleForceY = Random.Range(rubbleForceYMin, rubbleForceYMax);

                rubble.GetComponent<Rigidbody2D>().AddForce(new Vector2(rubbleForceX, rubbleForceY));
            }
        }
        Collider2D[] colliders = Physics2D.OverlapCircleAll(spawnPos, 99999999, LayerMask.GetMask("Block"));

        foreach (Collider2D c in colliders)
        {
            if (!c.gameObject.GetComponent<block>().startDestroyTimer && c.gameObject.transform.position.y > player.transform.position.y)
            {
                c.gameObject.GetComponent<Rigidbody2D>().WakeUp();
                c.gameObject.GetComponent<block>().sleepTimerClock = c.gameObject.GetComponent<block>().sleepTimer;
                c.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            }
        }

    }


    // Update is called once per frame
    //void Update () {        

    //}
}
