﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxGroundCheck : MonoBehaviour {
    private block block;

    public bool toldBox = false;

    public List<Collider2D> others = new List<Collider2D>();

    private void Start()
    {
        block = GetComponentInParent<block>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (others.Contains(null))
            others.Remove(null);

        if (others.Count == 0 && !toldBox)
        {
            //GetComponentInParent<Rigidbody2D>().WakeUp();
            //block.sleepTimerClock = block.sleepTimer;
            //GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            toldBox = true;
        }
        if (others.Count > 0)
        {
            toldBox = false;
        }
    }

    //other.tag != "spring"
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "block" || other.tag == "spring" || other.tag == "rubble")
        {
            others.Add(other);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "block" || other.tag == "spring" || other.tag == "rubble")
        {
            others.Remove(other);
        }
    }


}
