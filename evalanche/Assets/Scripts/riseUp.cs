﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class riseUp : MonoBehaviour {

    public GameObject lava;
    public float speed = 10;
    public float speedOffCam = 10;
    public float offCamDistance = 100;

    private bool jetpackStillInAir = false;

    // Update is called once per frame
    void Update () {
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().movementType == PlayerController.MovementType.jetpack)
        {
            jetpackStillInAir = true;
        }
        else if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().grounded
            && Mathf.Abs(GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().velocity.y) < 0.01)
        {
            jetpackStillInAir = false;
        }

        if (this.transform.position.y >= GameObject.FindGameObjectWithTag("MainCamera").transform.position.y - offCamDistance
            || jetpackStillInAir)
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.up * speedOffCam * Time.deltaTime);
        }
    }
}
