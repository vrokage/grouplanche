﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieTrigger : MonoBehaviour {

    public PlayerController player;

    void Start() {
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("block") || other.gameObject.CompareTag("spring"))// && other.gameObject.GetComponent <Rigidbody2D>().velocity.y < -.1)// && player.grounded == true)
        {
            if (player.grounded == true && !other.GetComponent<block>().isSafe && Mathf.Abs(player.GetComponent<Rigidbody2D>().velocity.y) < 0.01)
            {
                if (!transform.parent.gameObject.GetComponent<PlayerController>().shielded)
                {
                    transform.parent.gameObject.GetComponent<PlayerController>().Death();
                }
                else
                {
                    other.GetComponent<block>().Die();
                    transform.parent.gameObject.GetComponent<PlayerController>().shielded = false;
                }
            }
        }
    } 
}
