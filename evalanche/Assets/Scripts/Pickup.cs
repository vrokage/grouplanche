﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public enum PickupType { coin, jetpack, avalanche, coinMag, shield };
    public PickupType pickupType;
    public float magnetRadius = 10;

    private GameManager gameManager;
    private PlayerController playerController;
    private CubesFalling cubesFalling;
    private float origColliderRadius;

    private float DestroyTimer = 10f;
    private bool startDestroyTimer = false;

    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        cubesFalling = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<CubesFalling>();
        origColliderRadius = this.GetComponent<CircleCollider2D>().radius;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.y < -10)
            Destroy(this.gameObject);
        if (cubesFalling != null)
        {
            if (cubesFalling.coinMagActive)
            {
                this.GetComponent<CircleCollider2D>().radius = magnetRadius;
            }
            else
            {
                this.GetComponent<CircleCollider2D>().radius = origColliderRadius;
            }
        }

        if (startDestroyTimer)
            DestroyTimer -= Time.deltaTime;
        if (DestroyTimer <= 0)
            Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))// && other.gameObject.GetComponent <Rigidbody2D>().velocity.y < -.1)// && player.grounded == true)
        {
            if (pickupType == PickupType.coin)
            {
                if (GameObject.Find("GameManager") != null)
                    gameManager.coins += 1;
            }
            else if (pickupType == PickupType.jetpack)
            {
                if (playerController != null)
                {
                    playerController.movementType = PlayerController.MovementType.jetpack;//jetpackInitialBoost
                    playerController.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, playerController.jetpackInitialBoost));
                }
            }
            else if (pickupType == PickupType.avalanche)
            {
                if (cubesFalling != null)
                    cubesFalling.AvalanchePowerup();
            }
            else if (pickupType == PickupType.coinMag)
            {
                if (cubesFalling != null)
                    cubesFalling.CoinMagPowerup();
            }
            else if (pickupType == PickupType.shield)
            {
                if (playerController != null)
                    playerController.Shield();
            }

            Destroy(this.gameObject);
            /*
            // These lines should be pasted into the section where you want to cue/play the sound 
            deathEv = FMODUnity.RuntimeManager.CreateInstance(death);    // EventInstance is linked to the Event 
            deathEv.start();                                            // FINALLY... Play the sound!!

            GameOverMenu.enabled = true;
            Destroy(transform.parent.gameObject);
            */
        }

        else if (other.tag == "lava")
        {
            this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            startDestroyTimer = true;
        }
    }
}
