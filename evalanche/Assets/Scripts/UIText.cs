﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIText : MonoBehaviour {
    public string prefixStr = "X: ";
    public string suffixStr = "";
    private Text text;
    public enum DisplayType { score, highScore, coin };
    public DisplayType displayType;

    // Use this for initialization
    void Start () {
        text = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (displayType.Equals(DisplayType.score))
        {
            text.text = prefixStr + GameObject.Find("GameManager").GetComponent<GameManager>().prevScore.ToString("f0") + suffixStr;
        }
        if (displayType.Equals(DisplayType.highScore))
        {
            text.text = prefixStr + GameObject.Find("GameManager").GetComponent<GameManager>().highScore.ToString("f0") + suffixStr;
        }
        if (displayType.Equals(DisplayType.coin))
        {
            text.text = prefixStr + GameObject.Find("GameManager").GetComponent<GameManager>().coins.ToString("f0") + suffixStr;
        }
    }
}
