using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Camera2DFollow : MonoBehaviour
    {
        public Transform target;
        public float damping = 1;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;
		public bool arenaMode = true;
		public float yLockValue = 6.75f;
        public float yAddValue = .8f;
        public float xLockValue = 0f;

        public float yMinValue = 0f;
        public float yMaxValue = 5f; //For the top of the arena
        public float xMinValue = -1f; //For the left wall
        public float xMaxValue = 999999f; //For the right wall

        public Camera cam;
		 
		float nextTimeToSearch = 0;

        private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;
		private float tarY;
		//private bool moving = false;

        // Use this for initialization
        private void Start()
        {
            m_LastTargetPosition = target.position;
            m_CurrentPosition = transform.position;
            m_OffsetZ = (m_CurrentPosition - target.position).z;
            transform.SetParent(null);  //transform.parent = null;
			cam = GetComponent<Camera> ();
        }


        // Update is called once per frame
        private void FixedUpdate()
        {

			if (target == null) {
				FindPlayer ();
				return;
			}



            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (target.position - m_LastTargetPosition).x;
            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

            float yMoveDelta = (target.position - m_LastTargetPosition).y;
            if (arenaMode)
                updateLookAheadTarget = Mathf.Abs(yMoveDelta) > lookAheadMoveThreshold;

            if (updateLookAheadTarget)
            {
                if (!arenaMode)
                {
                    m_LookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
                }
                else
                {
                    m_LookAheadPos = lookAheadFactor * Vector3.up * Mathf.Sign(yMoveDelta);
                }
            }
            else
            {
                //if (!arenaMode)
                //{
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);
                //}
            }

            Vector3 aheadTargetPos = target.position + new Vector3(0, 0, 0) + m_LookAheadPos + Vector3.forward*m_OffsetZ;
            Vector3 newPos;
            if (!arenaMode)
            {
                newPos = Vector3.SmoothDamp(m_CurrentPosition, aheadTargetPos, ref m_CurrentVelocity, damping);
            }
            else
            {
                Vector3 velocity = Vector3.zero;
                newPos = Vector3.SmoothDamp(m_CurrentPosition, aheadTargetPos, ref velocity, damping);
            }

			
            //If not in arenaMode, then lock camera Y at yLockValue. If you are, then make camera Y the player's Y + yAddValue.
            if (arenaMode)
            {
                if (newPos.y + yAddValue <= yMaxValue)
                {
                    newPos = new Vector3(xLockValue, newPos.y, newPos.z);
                }
                else
                {
                    newPos = new Vector3(xLockValue, yMaxValue, newPos.z);
                }
            }
            else
            {
                if (newPos.x >= xMinValue)
                {
                    newPos = new Vector3(newPos.x, yLockValue, newPos.z);
                    if (newPos.x <= xMaxValue)
                    {
                        newPos = new Vector3(newPos.x, yLockValue, newPos.z);
                    }
                    else
                    {
                        newPos = new Vector3(xMaxValue, yLockValue, newPos.z);
                    }
                }
                else
                {
                    newPos = new Vector3(xMinValue, yLockValue, newPos.z);
                }
            }




            /*
			//Checks if player is out of screen bounds
			Vector3 playerPos = cam.WorldToScreenPoint(target.position);
			if (playerPos.y > cam.pixelHeight && !moving) {
				tarY = yLockValue;
				moving = true;
			} else if (playerPos.y < 0 && !moving) {
				tarY = yLockValue * -1;
				moving = true;
			}
            


			Vector3 curPos = cam.transform.position;

			//Moves camera gradually towards position, whole moving in x and z
			if (moving && tarY >= .15) {
				newPos = new Vector3 (newPos.x, curPos.y + .15f, newPos.z);
				tarY -= .15f;
			} else if (moving && tarY > 0) {
				newPos = new Vector3 (newPos.x, curPos.y + tarY, newPos.z);
				tarY = 0;
				moving = false;
			} else if (moving && tarY <= -.15) {
				newPos = new Vector3 (newPos.x, curPos.y - .15f, newPos.z);
				tarY += .15f;
			} else if (moving && tarY < 0) {
				newPos = new Vector3 (newPos.x, curPos.y - tarY, newPos.z);
				tarY = 0;
				moving = false;
			} else if (!moving) {
				newPos = new Vector3 (newPos.x, curPos.y, newPos.z);
			}
            */
            m_CurrentPosition = newPos;
            transform.position = newPos + new Vector3(0, yAddValue, 0);
            if (transform.position.y < yMinValue)
                transform.position = new Vector3(transform.position.x, yMinValue, transform.position.z);
            m_LastTargetPosition = newPos;
        }
        
		void FindPlayer () {
			if (nextTimeToSearch <= Time.time) {
				GameObject searchResult = GameObject.FindGameObjectWithTag ("Player");
				if (searchResult != null)
					target = searchResult.transform;
				nextTimeToSearch = Time.time + 0.5f;
			}
		}
	
    }
}
