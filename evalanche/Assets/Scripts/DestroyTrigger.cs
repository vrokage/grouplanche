﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTrigger : MonoBehaviour {

    public PlayerController player;

    //[FMODUnity.EventRef]
    //public string death = "event:/Death";   // string reference to the FMOD-authored Event named "Death"; name will appear in the Unity Inspector
    //FMOD.Studio.EventInstance deathEv;      // Unity EventInstance name for Death event that was created in FMOD

    void Start() {
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("block") || other.gameObject.CompareTag("spring") || other.gameObject.CompareTag("rubble"))// || other.gameObject.CompareTag("spring"))// && other.gameObject.GetComponent <Rigidbody2D>().velocity.y < -.1)// && player.grounded == true)
        {
            if (player.grounded == false)//GetComponentInParent<Rigidbody2D>().velocity.y > 0)
            {
                other.GetComponent<block>().blockHealthCurrent -= 1;
                if (other.GetComponent<block>().damagedMaterial != null)
                    other.GetComponent<MeshRenderer>().material = other.GetComponent<block>().damagedMaterial;
                GetComponentInParent<Rigidbody2D>().velocity = new Vector2(GetComponentInParent<Rigidbody2D>().velocity.x, 0);
            }
        }
    }


    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("block"))// || other.gameObject.CompareTag("spring"))// && other.gameObject.GetComponent <Rigidbody2D>().velocity.y < -.1)// && player.grounded == true)
        {
            if (player.grounded == false && other.GetComponent<block>().blockHealth <= 1)//GetComponentInParent<Rigidbody2D>().velocity.y > 0)
            {
                other.GetComponent<block>().blockHealthCurrent -= 1;
                if (other.GetComponent<block>().damagedMaterial != null)
                    other.GetComponent<MeshRenderer>().material = other.GetComponent<block>().damagedMaterial;
                GetComponentInParent<Rigidbody2D>().velocity = new Vector2(GetComponentInParent<Rigidbody2D>().velocity.x, 0);
            }
        }
    }
}
