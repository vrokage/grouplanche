﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grounded : MonoBehaviour {

    public bool isWallCheck = false;
    private PlayerController player;


	
	private void Start ()
    {

        player = GetComponentInParent<PlayerController>();

	}
    //other.tag != "spring"
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "block" || other.tag == "spring" || other.tag == "rubble")
        {
            if (isWallCheck && other.tag != "rubble")
            {
                player.touchingWall = true;
            }
            else
            {
                player.grounded = true;

                if (other.tag == "spring")
                    player.touchingSpring = true;
                else
                    player.touchingSpring = false;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "block" || other.tag == "spring" || other.tag == "rubble")
        {
            if (isWallCheck && other.tag != "rubble")
            {
                player.touchingWall = false;
            }
            else
            {
                player.grounded = false;

                if (other.tag == "spring")
                    player.touchingSpring = false;
            }
        }
    }
}
