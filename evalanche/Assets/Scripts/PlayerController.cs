﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;

    [HideInInspector] public bool grounded = false;
    [HideInInspector] public bool touchingWall = false;
    private bool wallJumped = false;
    private int wallJumpDirX = 0;
    public float wallJumpSpeed = 10;
    public float wallJumpXTime = 0.1f;
    private float wallJumpTimer = 0f;
    private float startingGravity;

    public GameObject player;
    public float playerSpeed = 10;
    //public float airSpeed = 200f;
    private bool facingRight = false;
    public int jumpHeight = 1250;
    public float inputDeadZone = 0.1f;
    //public int playerWallJumpHorzPower = 1250;
    private float moveX;
    public Canvas GameOverMenu;
    public Camera MainCamera;
    [HideInInspector] public bool touchingSpring = false;

    // Sound effects
    private AudioManager audioManager;
    public string deathSoundName = "Death";

    public enum MovementType { standard, jetpack };
    public MovementType movementType;
    [HideInInspector] public bool shielded = false;
    private bool triggeredShield = false;

    [FMODUnity.EventRef]
    public string jump_snd = "event:/Jump";   // string reference to the FMOD-authored Event named "Jump"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance jump_sndEv;      // Unity EventInstance name for Jump event that was created in FMOD

    [FMODUnity.EventRef]
    public string death = "event:/Death";   // string reference to the FMOD-authored Event named "Death"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance deathEv;      // Unity EventInstance name for Death event that was created in FMOD

    [HideInInspector] public bool playerIsDead = false;


    public float wallSlideSpeedMax = 3;
    public float jetpackForce = 50f;
    public float jetpackMaxSpeed = 50f;
    public float jetpackGravity = 10f;
    public float jetpackDuration = 5f;
    private float jetpackDurationClock;
    public float jetpackInitialBoost = 4000f;

    public float shieldDuration = 15f;
    private float shieldClock = 0;

    void Start() {
        rb = gameObject.GetComponent<Rigidbody2D>();
        startingGravity = rb.gravityScale;
        GameOverMenu.enabled = false;
        jetpackDurationClock = jetpackDuration;

        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("No AudioManager found in scene");
        }
    }

    void Update()
    {
        triggeredShield = false;
        if (!playerIsDead)
        {
            if (movementType == MovementType.standard)
            {
                rb.gravityScale = startingGravity;
                PlayerMove();
                PlayerJump();
            }
            else if (movementType == MovementType.jetpack)
            {
                rb.gravityScale = jetpackGravity;
                PlayerMove();
                Jetpack();
            }
        }

        //Powerup: Shield
        shieldClock -= Time.deltaTime;
        if (shieldClock < 0)
        {
            shielded = false;
        }
        if (shielded)
        {
            GetComponentInChildren<SpriteRenderer>().enabled = true;
        }
        else
        {
            GetComponentInChildren<SpriteRenderer>().enabled = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {        
        if (other.gameObject.CompareTag("lava"))
        {
            if (!shielded && !triggeredShield)
            {
                Death();
            }
            else
            {
                //rb.velocity = new Vector2(rb.velocity.x, 0);        
                rb.AddForce(new Vector2(0, jumpHeight));
                shielded = false;
                triggeredShield = true;
            }
        }
        if (other.gameObject.CompareTag("spikes"))
        {
            if (!shielded && !triggeredShield)
            {
                Death();
            }
            else
            {                
                other.transform.parent.GetComponent<block>().Die();
                shielded = false;
                triggeredShield = true;
                Destroy(other);
            }
        }

        if (other.gameObject.CompareTag("spring"))
        {
            touchingSpring = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("spring"))
        {
            touchingSpring = false;
        }
    }

    public void Death()
    {
        if (!playerIsDead && !shielded)
        {
            playerIsDead = true;
            this.GetComponent<BoxCollider2D>().enabled = false;
            this.GetComponent<MeshRenderer>().enabled = false;
            this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            foreach (BoxCollider2D c in this.GetComponentsInChildren<BoxCollider2D>())
            {
                c.enabled = false;
            }
            audioManager.PlaySound(deathSoundName);
            // These lines should be pasted into the section where you want to cue/play the sound 
            deathEv = FMODUnity.RuntimeManager.CreateInstance(death);    // EventInstance is linked to the Event 
            deathEv.start();                                            // FINALLY... Play the sound!!

            GameOverMenu.enabled = true;
        }
        else
        {
            shielded = false;
        }
    }

    void PlayerMove()
    {
        //CONTROLS
        moveX = Input.acceleration.x * 3;
        if (moveX > 1)
            moveX = 1;
        if (moveX < -1)
            moveX = -1;
        if (Mathf.Abs(moveX) < inputDeadZone)
            moveX = 0;

        if (Input.GetAxis("Horizontal") != 0f)
        {
            moveX = Input.GetAxis("Horizontal");
        }

        //PLAYER DIRECTION
        if (moveX < 0.0f && facingRight == false)
        {
            FlipPlayer();
        }
        else if (moveX > 0.0f && facingRight == true)
        {
            FlipPlayer();
        }

        //PHYSICS
        if (true)
        {
            Vector3 v3 = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
            if (wallJumped && movementType == MovementType.standard)
            {
                v3.x += wallJumpDirX * wallJumpSpeed * (wallJumpTimer / wallJumpXTime + 0.1f);
            }
            rb.velocity = v3;
        }
    }

    void PlayerJump()
    {
        int wallDirX = (!facingRight) ? -1 : 1;

        bool wallSliding = false;
        if (touchingWall && !grounded && rb.velocity.y < 0)
        {
            wallSliding = true;

            if (rb.velocity.y < -wallSlideSpeedMax)
            {
                rb.velocity = new Vector2(rb.velocity.x, -wallSlideSpeedMax);
            }
        }

        if (wallJumped)
        {
            wallJumpTimer -= Time.deltaTime;
            if (wallJumpTimer <= 0)
            {
                wallJumped = false;
                wallJumpTimer = wallJumpXTime;
            }
        }

        if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0))
        {
            if (wallSliding)
            {
                //if ((facingRight && wallDirX > 0) || (!facingRight && wallDirX < 0))
                wallJumped = true;
                wallJumpTimer = wallJumpXTime;
                wallJumpDirX = wallDirX;
                //GetComponent<Rigidbody2D>().AddForce(transform.right * wallDirX * wallJump.x);
                rb.AddForce(transform.up * jumpHeight);

                jump_sndEv = FMODUnity.RuntimeManager.CreateInstance(jump_snd);    // EventInstance is linked to the Event 
                jump_sndEv.start();                                            // FINALLY... Play the sound!!
            }
            if (grounded)
            {
                if (touchingSpring)
                {
                    rb.AddForce(new Vector2(0, jumpHeight * 1.5f));
                    touchingSpring = false;
                }
                else
                {
                    rb.AddForce(new Vector2(0, jumpHeight));
                }
                jump_sndEv = FMODUnity.RuntimeManager.CreateInstance(jump_snd);    // EventInstance is linked to the Event 
                jump_sndEv.start();                                            // FINALLY... Play the sound!!
            }

        }

        if (grounded)
        {
            wallJumped = false;
        }
    }

    void Jetpack()
    {
        if (Input.GetButton("Jump") || Input.GetMouseButton(0))
        {
            jetpackDurationClock -= Time.deltaTime;
            if (jetpackDurationClock <= 0)
            {
                jetpackDurationClock = jetpackDuration;
                movementType = MovementType.standard;
            }

            //Vector3 v3 = new Vector2(gameObject.GetComponent<Rigidbody2D>().velocity.x, jetpackForce);
            //rb.velocity = v3;
            rb.AddForce(new Vector2(0, jetpackForce));
            if (gameObject.GetComponent<Rigidbody2D>().velocity.y > jetpackMaxSpeed)
            {
                rb.velocity = new Vector3(gameObject.GetComponent<Rigidbody2D>().velocity.x, jetpackMaxSpeed);
            }
        }
    }

    public void Shield()
    {
        shielded = true;
        shieldClock = shieldDuration;
    }

    void FlipPlayer() {
        facingRight = !facingRight;
        Vector3 localScale = gameObject.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }
}