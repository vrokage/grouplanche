﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubesFalling : MonoBehaviour {
    private float spawnAreaLeft = -18;
    private float spawnAreaRight = 18;

    public GameObject blockStandard;
    public GameObject [] blocks;
    public GameObject [] powerups;
    public float delayBlock = 0.75f;
    private float delayBlockClock; //private timer
    public float delayBlockSpecial = 1f;
    private float delayBlockSpecialClock; //private timer
    public float delayPowerup = 30f;
    private float delayPowerupClock; //private timer

    private float heightDifference = 0f;
    private Collider2D[] colliders;

    //Power Up: Blocks fall faster
    public float delayBlockAvalanche = 0.5f;
    private float delayBlockAvalancheClock; //private timer
    public float avalancheDuration = 10f;
    public float avalancheClock; //private timer

    //Power Up: Coin Magnet
    public bool coinMagActive = false;
    public float coinMagDuration = 5f;
    private float coinMagClock; //private timer

    // Use this for initialization
    void Start () {
        delayPowerupClock = delayPowerup;
        delayBlockClock = delayBlock;

        delayBlockAvalancheClock = delayBlockAvalanche;
        avalancheClock = 0;
        coinMagClock = 0;
        //InvokeRepeating("SpawnPowerup", delayPowerup, delayPowerup);
    }

    private void Update()
    {
        delayPowerupClock -= Time.deltaTime;
        if (delayPowerupClock <= 0)
        {
            delayPowerupClock = delayPowerup + Random.Range(-delayPowerup/2, delayPowerup/2);
            SpawnPowerup();
        }

        delayBlockClock -= Time.deltaTime;
        if (delayBlockClock <= 0 && !GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().playerIsDead)
        {
            delayBlockClock = delayBlock;
            SpawnBlock();
        }

        delayBlockSpecialClock -= Time.deltaTime;
        if (delayBlockSpecialClock <= 0 && !GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().playerIsDead)
        {
            delayBlockSpecialClock = delayBlockSpecial;
            SpawnBlockSpecial();
        }

        //Powerup: Avalanche
        avalancheClock -= Time.deltaTime;        
        if (avalancheClock > 0)
        {
            delayBlockAvalancheClock -= Time.deltaTime;
        }
        else
        {
            delayBlockAvalancheClock = delayBlockAvalanche;
        }
        if (delayBlockAvalancheClock <= 0 && !GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().playerIsDead)
        {
            delayBlockAvalancheClock = delayBlockAvalanche;
            spawnWithoutOverlap(blockStandard, Camera.main.transform.position.y + 75);            
        }

        //Powerup: Coin Magnet
        coinMagClock -= Time.deltaTime;
        if (coinMagClock > 0)
        {
            coinMagActive = true;
        }
        else
        {
            coinMagActive = false;
        }
    }

    void SpawnBlock()
    {

        if (75 + heightDifference > Camera.main.transform.position.y + 45)
        {
            heightDifference += 5;
            spawnWithoutOverlap(blockStandard, 75 + heightDifference);
        }
        else
        {
            heightDifference += 5;
            spawnWithoutOverlap(blockStandard, Camera.main.transform.position.y + 75);
            //heightDifference = Camera.main.transform.position.y + 45;
        }
    }

    void SpawnBlockSpecial()
    {
        GameObject block = blocks[Random.Range(0, blocks.Length)];
        spawnWithoutOverlap(block, Camera.main.transform.position.y + 150);
    }

    void SpawnPowerup () {
        GameObject block = powerups[Random.Range(0, powerups.Length)];
        spawnWithoutOverlap(block, Camera.main.transform.position.y + 150);
    }

    void spawnWithoutOverlap(GameObject spawnObj, float spawnHeight)
    {
        float spawnPosX = Random.Range(spawnAreaLeft, spawnAreaRight);
        bool canSpawnHere = isSpawnOverlap(new Vector2(spawnPosX, spawnHeight), spawnObj.GetComponent<Collider2D>().bounds.extents);

        int failsafe = 50;
        while (!canSpawnHere && failsafe > 0)
        {
            spawnPosX = Random.Range(spawnAreaLeft, spawnAreaRight);
            canSpawnHere = isSpawnOverlap(new Vector2(spawnPosX, spawnHeight), spawnObj.GetComponent<Collider2D>().bounds.extents);
            failsafe--;
        }

        if (canSpawnHere)
        {
            Instantiate(spawnObj, new Vector3(spawnPosX, spawnHeight, 0), Quaternion.identity);
        }

    }

    bool isSpawnOverlap(Vector2 spawnPos, Vector2 spawnExtent)
    {
        colliders = Physics2D.OverlapCircleAll(spawnPos, spawnAreaRight * 2);

        foreach(Collider2D c in colliders)
        {
            Vector2 centerPoint = c.bounds.center;
            float width = c.bounds.extents.x;
            float height = c.bounds.extents.y;

            float leftExtent = centerPoint.x - width;
            float rightExtent = centerPoint.x + width;
            float lowerExtent = centerPoint.y - width;
            float upperExtent = centerPoint.y + width;

            if ((spawnPos.x + spawnExtent.x >= leftExtent && spawnPos.x + spawnExtent.x <= rightExtent) 
                || (spawnPos.x - spawnExtent.x >= leftExtent && spawnPos.x - spawnExtent.x <= rightExtent))
            {
                if ((spawnPos.y + spawnExtent.y >= lowerExtent && spawnPos.y + spawnExtent.y <= upperExtent)
                    || (spawnPos.y - spawnExtent.y >= lowerExtent && spawnPos.y - spawnExtent.y <= upperExtent))
                {
                    return false;
                }
            }
        }
        return true;
    }

    public void AvalanchePowerup()
    {
        avalancheClock = avalancheDuration;
    }

    public void CoinMagPowerup()
    {
        coinMagClock = coinMagDuration;
    }
}
