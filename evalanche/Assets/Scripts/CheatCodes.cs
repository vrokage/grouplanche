﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheatCodes : MonoBehaviour {
    CubesFalling cubesFalling;
    public bool cheatsEnabled = true;
    [Header("Power Up Keys")]
    public string jetpack = "1";
    public string avalanche = "2";
    public string shield = "3";
    public string coinMagnet = "4";




    // Use this for initialization
    void Start () {
        cubesFalling = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<CubesFalling>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!cheatsEnabled)
            Destroy(this.gameObject);
		if(Input.GetKeyDown(jetpack))
        {
            Instantiate(cubesFalling.powerups[0], new Vector3(Random.Range(-20, 20), Camera.main.transform.position.y + 45, -0.1f), Quaternion.identity);
        }
        if (Input.GetKeyDown(avalanche))
        {
            Instantiate(cubesFalling.powerups[1], new Vector3(Random.Range(-20, 20), Camera.main.transform.position.y + 45, -0.1f), Quaternion.identity);
        }
        if (Input.GetKeyDown(shield))
        {
            Instantiate(cubesFalling.powerups[2], new Vector3(Random.Range(-20, 20), Camera.main.transform.position.y + 45, -0.1f), Quaternion.identity);
        }
        if (Input.GetKeyDown(coinMagnet))
        {
            Instantiate(cubesFalling.powerups[3], new Vector3(Random.Range(-20, 20), Camera.main.transform.position.y + 45, -0.1f), Quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
